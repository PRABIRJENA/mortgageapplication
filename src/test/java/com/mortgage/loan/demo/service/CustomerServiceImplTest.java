package com.mortgage.loan.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.mortgage.loan.demo.constants.AppConstants;
import com.mortgage.loan.demo.dto.CustomerLoginReqDto;
import com.mortgage.loan.demo.dto.CustomerLoginResponseDto;
import com.mortgage.loan.demo.entity.Customer;
import com.mortgage.loan.demo.repository.CustomerRepository;

class CustomerServiceImplTest {

	@InjectMocks
	CustomerServiceImpl customerServiceImpl;

	@Mock
	CustomerRepository customerRepository;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testLoginValidationSuccess() {
		CustomerLoginReqDto customerLoginReqDto = new CustomerLoginReqDto();
		customerLoginReqDto.setCustomerId("PKJ100");
		customerLoginReqDto.setPassword("123456");

		Customer customer = new Customer();
		customer.setCustomerId("PKJ100");
		customer.setPassword("123456");
		when(customerRepository.findByCustomerIdAndPassword(customer.getCustomerId(), customer.getPassword()))
				.thenReturn(customer);

		CustomerLoginResponseDto customerLoginResponseDto = customerServiceImpl.customerLogin(customerLoginReqDto);
		assertEquals(AppConstants.LOGIN_SUCCESS, customerLoginResponseDto.getStatusMsg());
		assertEquals(AppConstants.LOGIN_SUCCESS_STATUS_CODE, customerLoginResponseDto.getStatusCode());
	}

	@Test
	public void testLoginValidationFailure() {
		CustomerLoginReqDto customerLoginReqDto = new CustomerLoginReqDto();
		customerLoginReqDto.setCustomerId("PKJ100");
		customerLoginReqDto.setPassword("123456");

		Customer customer = new Customer();
		customer.setCustomerId("PKJ100");
		customer.setPassword("1234561");
		when(customerRepository.findByCustomerIdAndPassword(customer.getCustomerId(), customer.getPassword()))
				.thenReturn(customer);

		CustomerLoginResponseDto customerLoginResponseDto = customerServiceImpl.customerLogin(customerLoginReqDto);
		assertEquals(AppConstants.LOGIN_FAILURE, customerLoginResponseDto.getStatusMsg());
		assertEquals(AppConstants.LOGIN_FAILURE_STATUS_CODE, customerLoginResponseDto.getStatusCode());
	}

}
