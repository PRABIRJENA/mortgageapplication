package com.mortgage.loan.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.mortgage.loan.demo.dto.TransactionDetailsResponseDTO;
import com.mortgage.loan.demo.dto.TransactionSummaryResponseDTO;
import com.mortgage.loan.demo.entity.Customer;
import com.mortgage.loan.demo.entity.CustomerSavingAccount;
import com.mortgage.loan.demo.entity.MortgageAccount;
import com.mortgage.loan.demo.entity.MortgagePropertyDetails;
import com.mortgage.loan.demo.entity.TransactionDetails;
import com.mortgage.loan.demo.repository.CustomerRepository;
import com.mortgage.loan.demo.repository.CustomerSavingAccountRepository;
import com.mortgage.loan.demo.repository.MortgageAccountRepository;
import com.mortgage.loan.demo.repository.MortgagePropertyDetailsRepository;
import com.mortgage.loan.demo.repository.TransactionDetailsRepository;

public class TransactionSummaryServiceImplTest {

	@InjectMocks
	TransactionSummaryServiceImpl transactionSummaryServiceImpl;

	@Mock
	TransactionDetailsRepository transactionDetailsRepository;

	@Mock
	MortgageAccountRepository mortgageAccountRepository;

	@Mock
	MortgagePropertyDetailsRepository mortgagePropertyDetailsRepository;

	@Mock
	CustomerRepository customerRepository;

	@Mock
	CustomerSavingAccountRepository customerSavingAccountRepository;

	private TransactionSummaryResponseDTO transactionSummaryResponseDTO;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetTransactionDetails() {
		Customer customer = new Customer();
		customer.setCustId(1);
		customer.setCustomerId("PKJ100");
		customer.setEmail("prabir@gmail.com");
		customer.setCustomerEmployeeType("employed");
		customer.setMobileNumber("1232312312");

		MortgageAccount mortgageAccountSummary = new MortgageAccount();
		mortgageAccountSummary.setMortgageAccId(1);
		mortgageAccountSummary.setAccountBalance(100000.0);
		mortgageAccountSummary.setCreatedDate(LocalDateTime.now());
		mortgageAccountSummary.setCustomer(customer);
		mortgageAccountSummary.setMortgageAccNumber("MTG100");

		MortgagePropertyDetails mortgagePropertyDetails = new MortgagePropertyDetails();
		mortgagePropertyDetails.setComment("Land Loan");
		mortgagePropertyDetails.setMortgageProId(1);
		mortgagePropertyDetails.setMortgageAccount(mortgageAccountSummary);

		TransactionDetails transactionDetails = new TransactionDetails();
		transactionDetails.setTransactionAccId(1);
		transactionDetails.setTransactionalAccBalance(20000.0);
		transactionDetails.setTranscationDate(LocalDateTime.now());
		transactionDetails.setTransComment("Land Loan");
		transactionDetails.setCustomer(customer);
		transactionDetails.setTransactionType("Credit Card");

		TransactionDetails transactionDetails1 = new TransactionDetails();
		transactionDetails1.setTransactionAccId(2);
		transactionDetails1.setTransactionalAccBalance(30000.0);
		transactionDetails1.setTranscationDate(LocalDateTime.now().plusMinutes(2));
		transactionDetails1.setCustomer(customer);
		transactionDetails1.setTransComment("Land Loan");
		transactionDetails1.setTransactionType("Credit Card");

		TransactionDetails transactionDetails3 = new TransactionDetails();
		transactionDetails3.setTransactionAccId(3);
		transactionDetails3.setTransactionalAccBalance(10000.0);
		transactionDetails3.setTranscationDate(LocalDateTime.now());
		transactionDetails3.setTransComment("Land Loan");
		transactionDetails3.setCustomer(customer);
		transactionDetails3.setTransactionType("Credit Card");

		TransactionDetails transactionDetails4 = new TransactionDetails();
		transactionDetails4.setTransactionAccId(4);
		transactionDetails4.setTransactionalAccBalance(30000.0);
		transactionDetails4.setTranscationDate(LocalDateTime.now().plusMinutes(4));
		transactionDetails4.setCustomer(customer);
		transactionDetails4.setTransComment("Land Loan");
		transactionDetails4.setTransactionType("Credit Card");

		List<TransactionDetails> transactionDetailsList = new ArrayList<>();
		transactionDetailsList.add(transactionDetails);
		transactionDetailsList.add(transactionDetails1);
		transactionDetailsList.add(transactionDetails3);
		transactionDetailsList.add(transactionDetails4);

		when(customerRepository.findByCustomerId("PKJ100")).thenReturn(customer);
		when(mortgageAccountRepository.findByCustomer(customer)).thenReturn(mortgageAccountSummary);
		when(transactionDetailsRepository.findByCustId(customer.getCustId())).thenReturn(transactionDetailsList);
		when(mortgagePropertyDetailsRepository.findByMortgageAccount(mortgageAccountSummary))
				.thenReturn(mortgagePropertyDetails);
		List<TransactionDetailsResponseDTO> transactionSummaryResponseDTOs = new ArrayList<>();
		transactionSummaryResponseDTOs = transactionSummaryServiceImpl.getTransactionDetails("PKJ100");
		assertEquals(4, transactionSummaryResponseDTOs.size());
		assertEquals(100000.0, transactionSummaryResponseDTOs.get(0).getMortgageAccountBalance());

	}

	@Test
	public void testGetTransactionDetailsSummary() {
		Customer customer = new Customer();
		customer.setCustId(1);
		customer.setCustomerId("PKJ100");
		customer.setEmail("prabir@gmail.com");
		customer.setCustomerEmployeeType("employed");
		customer.setMobileNumber("1232312312");

		CustomerSavingAccount customerSavingAccount = new CustomerSavingAccount();
		customerSavingAccount.setCustomerSavingAccId(1);
		customerSavingAccount.setAccountType("Savings Account");
		customerSavingAccount.setBalance(100000.0);
		customerSavingAccount.setCreatedDate(LocalDateTime.now());
		customerSavingAccount.setCustomerAccountNumber("1");
		customerSavingAccount.setCustomer(customer);

		MortgageAccount mortgageAccountSummary = new MortgageAccount();
		mortgageAccountSummary.setMortgageAccId(1);
		mortgageAccountSummary.setAccountBalance(100000.0);
		mortgageAccountSummary.setCreatedDate(LocalDateTime.now());
		mortgageAccountSummary.setCustomer(customer);
		mortgageAccountSummary.setMortgageAccNumber("MTG100");
		when(customerRepository.findByCustomerId("PKJ100")).thenReturn(customer);
		when(customerSavingAccountRepository.findByCustomer(customer)).thenReturn(customerSavingAccount);
		when(mortgageAccountRepository.findByCustomer(customer)).thenReturn(mortgageAccountSummary);

		transactionSummaryResponseDTO = transactionSummaryServiceImpl.getTransactionsSummary("PKJ100");

		assertEquals("MTG100", transactionSummaryResponseDTO.getMortgageAccNumber());
		assertEquals(100000.0, transactionSummaryResponseDTO.getAccountBalance());

	}

	@Test
	public void testGetTransactionDetailsSummaryFailure() {
		Customer customer = new Customer();
		customer.setCustId(1);
		customer.setCustomerId("PKJ100");
		customer.setEmail("prabir@gmail.com");
		customer.setCustomerEmployeeType("employed");
		customer.setMobileNumber("1232312312");

		CustomerSavingAccount customerSavingAccount = new CustomerSavingAccount();
		customerSavingAccount.setCustomerSavingAccId(1);
		customerSavingAccount.setAccountType("Savings Account");
		customerSavingAccount.setBalance(100000.0);
		customerSavingAccount.setCreatedDate(LocalDateTime.now());
		customerSavingAccount.setCustomerAccountNumber("1");
		customerSavingAccount.setCustomer(customer);

		MortgageAccount mortgageAccountSummary = new MortgageAccount();
		mortgageAccountSummary.setMortgageAccId(1);
		mortgageAccountSummary.setAccountBalance(100000.0);
		mortgageAccountSummary.setCreatedDate(LocalDateTime.now());
		mortgageAccountSummary.setCustomer(customer);
		mortgageAccountSummary.setMortgageAccNumber("MTG100");
		when(customerRepository.findByCustomerId("PKJ100")).thenReturn(null);
		when(customerSavingAccountRepository.findByCustomer(customer)).thenReturn(customerSavingAccount);
		when(mortgageAccountRepository.findByCustomer(customer)).thenReturn(mortgageAccountSummary);

		transactionSummaryResponseDTO = transactionSummaryServiceImpl.getTransactionsSummary("PKJ100");

		assertEquals(null, transactionSummaryResponseDTO.getMortgageAccNumber());
		assertEquals(0.0, transactionSummaryResponseDTO.getAccountBalance());

	}

	@Test
	public void testGetTransactionDetailsSummaryFailure1() {
		Customer customer = new Customer();
		customer.setCustId(1);
		customer.setCustomerId("PKJ100");
		customer.setEmail("prabir@gmail.com");
		customer.setCustomerEmployeeType("employed");
		customer.setMobileNumber("1232312312");

		CustomerSavingAccount customerSavingAccount = new CustomerSavingAccount();
		customerSavingAccount.setCustomerSavingAccId(1);
		customerSavingAccount.setAccountType("Savings Account");
		customerSavingAccount.setBalance(100000.0);
		customerSavingAccount.setCreatedDate(LocalDateTime.now());
		customerSavingAccount.setCustomerAccountNumber("1");
		customerSavingAccount.setCustomer(customer);

		MortgageAccount mortgageAccountSummary = new MortgageAccount();
		mortgageAccountSummary.setMortgageAccId(1);
		mortgageAccountSummary.setAccountBalance(100000.0);
		mortgageAccountSummary.setCreatedDate(LocalDateTime.now());
		mortgageAccountSummary.setCustomer(customer);
		mortgageAccountSummary.setMortgageAccNumber("MTG100");
		when(customerRepository.findByCustomerId("PKJ100")).thenReturn(customer);
		when(customerSavingAccountRepository.findByCustomer(customer)).thenReturn(null);
		when(mortgageAccountRepository.findByCustomer(customer)).thenReturn(mortgageAccountSummary);

		transactionSummaryResponseDTO = transactionSummaryServiceImpl.getTransactionsSummary("PKJ100");

		assertEquals("MTG100", transactionSummaryResponseDTO.getMortgageAccNumber());
		assertEquals(100000.0, transactionSummaryResponseDTO.getAccountBalance());

	}

	@Test
	public void testGetTransactionDetailsSummaryFailure2() {
		Customer customer = new Customer();
		customer.setCustId(1);
		customer.setCustomerId("PKJ100");
		customer.setEmail("prabir@gmail.com");
		customer.setCustomerEmployeeType("employed");
		customer.setMobileNumber("1232312312");

		CustomerSavingAccount customerSavingAccount = new CustomerSavingAccount();
		customerSavingAccount.setCustomerSavingAccId(1);
		customerSavingAccount.setAccountType("Savings Account");
		customerSavingAccount.setBalance(100000.0);
		customerSavingAccount.setCreatedDate(LocalDateTime.now());
		customerSavingAccount.setCustomerAccountNumber("1");
		customerSavingAccount.setCustomer(customer);

		MortgageAccount mortgageAccountSummary = new MortgageAccount();
		mortgageAccountSummary.setMortgageAccId(1);
		mortgageAccountSummary.setAccountBalance(100000.0);
		mortgageAccountSummary.setCreatedDate(LocalDateTime.now());
		mortgageAccountSummary.setCustomer(customer);
		mortgageAccountSummary.setMortgageAccNumber("MTG100");
		when(customerRepository.findByCustomerId("PKJ100")).thenReturn(customer);
		when(customerSavingAccountRepository.findByCustomer(customer)).thenReturn(customerSavingAccount);
		when(mortgageAccountRepository.findByCustomer(customer)).thenReturn(null);

		transactionSummaryResponseDTO = transactionSummaryServiceImpl.getTransactionsSummary("PKJ100");

		assertEquals(null, transactionSummaryResponseDTO.getMortgageAccNumber());
		assertEquals(0.0, transactionSummaryResponseDTO.getAccountBalance());

	}
}
