package com.mortgage.loan.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.mortgage.loan.demo.constants.AppConstants;
import com.mortgage.loan.demo.dto.CustomerLoginReqDto;
import com.mortgage.loan.demo.dto.CustomerLoginResponseDto;
import com.mortgage.loan.demo.dto.TransactionDetailsResponseDTO;
import com.mortgage.loan.demo.dto.TransactionSummaryResponseDTO;
import com.mortgage.loan.demo.entity.Customer;
import com.mortgage.loan.demo.entity.CustomerSavingAccount;
import com.mortgage.loan.demo.entity.MortgageAccount;
import com.mortgage.loan.demo.entity.MortgagePropertyDetails;
import com.mortgage.loan.demo.entity.TransactionDetails;
import com.mortgage.loan.demo.repository.CustomerRepository;
import com.mortgage.loan.demo.repository.CustomerSavingAccountRepository;
import com.mortgage.loan.demo.repository.MortgageAccountRepository;
import com.mortgage.loan.demo.repository.MortgagePropertyDetailsRepository;
import com.mortgage.loan.demo.repository.TransactionDetailsRepository;
import com.mortgage.loan.demo.service.CustomerServiceImpl;
import com.mortgage.loan.demo.service.TransactionSummaryServiceImpl;

public class CustomerControllerTest {

	@InjectMocks
	CustomerController customerController;

	@Mock
	CustomerServiceImpl customerServiceImpl;

	@Mock
	TransactionSummaryServiceImpl transactionSummaryServiceImpl;

	@Mock
	TransactionDetailsRepository transactionDetailsRepository;

	@Mock
	MortgageAccountRepository mortgageAccountRepository;

	@Mock
	MortgagePropertyDetailsRepository mortgagePropertyDetailsRepository;

	@Mock
	CustomerRepository customerRepository;

	@Mock
	CustomerSavingAccountRepository customerSavingAccountRepository;

	private TransactionSummaryResponseDTO transactionSummaryResponseDTO;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void userLoginTest() {
		CustomerLoginReqDto customerLoginReqDto = new CustomerLoginReqDto();
		CustomerLoginResponseDto customerLoginResponseDto = new CustomerLoginResponseDto();
		customerLoginReqDto.setCustomerId("PKJ100");
		customerLoginReqDto.setPassword("123456");
		customerLoginResponseDto.setStatusCode(AppConstants.LOGIN_SUCCESS_STATUS_CODE);
		customerLoginResponseDto.setStatusMsg(AppConstants.LOGIN_SUCCESS);
		doReturn(customerLoginResponseDto).when(customerServiceImpl).customerLogin(customerLoginReqDto);
		ResponseEntity<CustomerLoginResponseDto> customerLoginResponseDto1 = customerController
				.customerLogin(customerLoginReqDto);

		assertEquals(AppConstants.LOGIN_SUCCESS, customerLoginResponseDto1.getBody().getStatusMsg());
		assertEquals(AppConstants.LOGIN_SUCCESS_STATUS_CODE, customerLoginResponseDto1.getBody().getStatusCode());
	}

	@Test
	public void userLoginTestFailure() {
		CustomerLoginReqDto customerLoginReqDto = new CustomerLoginReqDto();
		CustomerLoginResponseDto customerLoginResponseDto = new CustomerLoginResponseDto();
		customerLoginResponseDto.setStatusCode(AppConstants.LOGIN_FAILURE_STATUS_CODE);
		customerLoginResponseDto.setStatusMsg(AppConstants.LOGIN_FAILURE);
		doReturn(customerLoginResponseDto).when(customerServiceImpl).customerLogin(customerLoginReqDto);
		ResponseEntity<CustomerLoginResponseDto> customerLoginResponseDto1 = customerController
				.customerLogin(customerLoginReqDto);

		assertEquals(AppConstants.LOGIN_FAILURE, customerLoginResponseDto1.getBody().getStatusMsg());
		assertEquals(AppConstants.LOGIN_FAILURE_STATUS_CODE, customerLoginResponseDto1.getBody().getStatusCode());
	}

	@Test
	public void testGetTransactionDetails() {
		Customer customer = new Customer();
		customer.setCustId(1);
		customer.setCustomerId("PKJ100");
		customer.setEmail("prabir@gmail.com");
		customer.setCustomerEmployeeType("employed");
		customer.setMobileNumber("1232312312");

		MortgageAccount mortgageAccountSummary = new MortgageAccount();
		mortgageAccountSummary.setMortgageAccId(1);
		mortgageAccountSummary.setAccountBalance(100000.0);
		mortgageAccountSummary.setCreatedDate(LocalDateTime.now());
		mortgageAccountSummary.setCustomer(customer);
		mortgageAccountSummary.setMortgageAccNumber("MTG100");

		MortgagePropertyDetails mortgagePropertyDetails = new MortgagePropertyDetails();
		mortgagePropertyDetails.setComment("Land Loan");
		mortgagePropertyDetails.setMortgageProId(1);
		mortgagePropertyDetails.setMortgageAccount(mortgageAccountSummary);

		TransactionDetails transactionDetails = new TransactionDetails();
		transactionDetails.setTransactionAccId(1);
		transactionDetails.setTransactionalAccBalance(20000.0);
		transactionDetails.setTranscationDate(LocalDateTime.now());
		transactionDetails.setTransComment("Land Loan");
		transactionDetails.setCustomer(customer);
		transactionDetails.setTransactionType("Credit Card");

		TransactionDetails transactionDetails1 = new TransactionDetails();
		transactionDetails1.setTransactionAccId(2);
		transactionDetails1.setTransactionalAccBalance(30000.0);
		transactionDetails1.setTranscationDate(LocalDateTime.now().plusMinutes(2));
		transactionDetails1.setCustomer(customer);
		transactionDetails1.setTransComment("Land Loan");
		transactionDetails1.setTransactionType("Credit Card");

		TransactionDetails transactionDetails3 = new TransactionDetails();
		transactionDetails3.setTransactionAccId(3);
		transactionDetails3.setTransactionalAccBalance(10000.0);
		transactionDetails3.setTranscationDate(LocalDateTime.now());
		transactionDetails3.setTransComment("Land Loan");
		transactionDetails3.setCustomer(customer);
		transactionDetails3.setTransactionType("Credit Card");

		TransactionDetails transactionDetails4 = new TransactionDetails();
		transactionDetails4.setTransactionAccId(4);
		transactionDetails4.setTransactionalAccBalance(30000.0);
		transactionDetails4.setTranscationDate(LocalDateTime.now().plusMinutes(4));
		transactionDetails4.setCustomer(customer);
		transactionDetails4.setTransComment("Land Loan");
		transactionDetails4.setTransactionType("Credit Card");

		List<TransactionDetails> transactionDetailsList = new ArrayList<>();
		transactionDetailsList.add(transactionDetails);
		transactionDetailsList.add(transactionDetails1);
		transactionDetailsList.add(transactionDetails3);
		transactionDetailsList.add(transactionDetails4);

		when(customerRepository.findByCustomerId("PKJ100")).thenReturn(customer);
		when(mortgageAccountRepository.findByCustomer(customer)).thenReturn(mortgageAccountSummary);
		when(transactionDetailsRepository.findByCustId(customer.getCustId())).thenReturn(transactionDetailsList);
		when(mortgagePropertyDetailsRepository.findByMortgageAccount(mortgageAccountSummary))
				.thenReturn(mortgagePropertyDetails);

		TransactionDetailsResponseDTO transactionDetailsResponseDTO = new TransactionDetailsResponseDTO();
		transactionDetailsResponseDTO.setMortgageAccountBalance(100000.0);
		transactionDetailsResponseDTO.setMortgageComment("Land loan");
		transactionDetailsResponseDTO.setMortgageCreatedDate(LocalDateTime.now());
		transactionDetailsResponseDTO.setTransactionDate(LocalDateTime.now());
		transactionDetailsResponseDTO.setTransComment("transaction Succesful");

		TransactionDetailsResponseDTO transactionDetailsResponseDTO2 = new TransactionDetailsResponseDTO();
		transactionDetailsResponseDTO2.setMortgageAccountBalance(500000.0);
		transactionDetailsResponseDTO2.setMortgageComment("Land loan");
		transactionDetailsResponseDTO2.setMortgageCreatedDate(LocalDateTime.now());
		transactionDetailsResponseDTO2.setTransactionDate(LocalDateTime.now());
		transactionDetailsResponseDTO2.setTransComment("transaction Succesful");

		List<TransactionDetailsResponseDTO> transactionSummaryResponseDTOs = new ArrayList<>();
		transactionSummaryResponseDTOs.add(transactionDetailsResponseDTO);
		transactionSummaryResponseDTOs.add(transactionDetailsResponseDTO2);
		when(transactionSummaryServiceImpl.getTransactionDetails("PKJ100")).thenReturn(transactionSummaryResponseDTOs);
		ResponseEntity<List<TransactionDetailsResponseDTO>> transactionSummaryResponseDTOs2 = customerController
				.customerTransactionDetails("PKJ100");
		assertEquals(200, transactionSummaryResponseDTOs2.getStatusCodeValue());
		assertEquals(HttpStatus.OK, transactionSummaryResponseDTOs2.getStatusCode());

	}

	@Test
	public void testGetTransactionDetailsSummary() {
		Customer customer = new Customer();
		customer.setCustId(1);
		customer.setCustomerId("PKJ100");
		customer.setEmail("prabir@gmail.com");
		customer.setCustomerEmployeeType("employed");
		customer.setMobileNumber("1232312312");

		CustomerSavingAccount customerSavingAccount = new CustomerSavingAccount();
		customerSavingAccount.setCustomerSavingAccId(1);
		customerSavingAccount.setAccountType("Savings Account");
		customerSavingAccount.setBalance(100000.0);
		customerSavingAccount.setCreatedDate(LocalDateTime.now());
		customerSavingAccount.setCustomerAccountNumber("1");
		customerSavingAccount.setCustomer(customer);

		MortgageAccount mortgageAccountSummary = new MortgageAccount();
		mortgageAccountSummary.setMortgageAccId(1);
		mortgageAccountSummary.setAccountBalance(100000.0);
		mortgageAccountSummary.setCreatedDate(LocalDateTime.now());
		mortgageAccountSummary.setCustomer(customer);
		mortgageAccountSummary.setMortgageAccNumber("MTG100");
		when(customerRepository.findByCustomerId("PKJ100")).thenReturn(customer);
		when(customerSavingAccountRepository.findByCustomer(customer)).thenReturn(customerSavingAccount);
		when(mortgageAccountRepository.findByCustomer(customer)).thenReturn(mortgageAccountSummary);
		transactionSummaryResponseDTO = new TransactionSummaryResponseDTO();
		transactionSummaryResponseDTO.setMortgageAccNumber("1");
		transactionSummaryResponseDTO.setAccountBalance(50000.0);
		transactionSummaryResponseDTO.setTransactionalAccBalance(20000.0);

		when(transactionSummaryServiceImpl.getTransactionsSummary("PKJ100")).thenReturn(transactionSummaryResponseDTO);
		ResponseEntity<TransactionSummaryResponseDTO> transactionSummaryResponseDTO = customerController
				.customerTransactionSummary("PKJ100");

		assertEquals(200, transactionSummaryResponseDTO.getStatusCodeValue());
		assertEquals(HttpStatus.OK, transactionSummaryResponseDTO.getStatusCode());

	}

	@Test
	public void testGetTransactionDetailsSummaryFailure() {
		Customer customer = new Customer();
		customer.setCustId(1);
		customer.setCustomerId("PKJ100");
		customer.setEmail("prabir@gmail.com");
		customer.setCustomerEmployeeType("employed");
		customer.setMobileNumber("1232312312");

		CustomerSavingAccount customerSavingAccount = new CustomerSavingAccount();
		customerSavingAccount.setCustomerSavingAccId(1);
		customerSavingAccount.setAccountType("Savings Account");
		customerSavingAccount.setBalance(100000.0);
		customerSavingAccount.setCreatedDate(LocalDateTime.now());
		customerSavingAccount.setCustomerAccountNumber("1");
		customerSavingAccount.setCustomer(customer);

		MortgageAccount mortgageAccountSummary = new MortgageAccount();
		mortgageAccountSummary.setMortgageAccId(1);
		mortgageAccountSummary.setAccountBalance(100000.0);
		mortgageAccountSummary.setCreatedDate(LocalDateTime.now());
		mortgageAccountSummary.setCustomer(customer);
		mortgageAccountSummary.setMortgageAccNumber("MTG100");
		when(customerRepository.findByCustomerId("PKJ100")).thenReturn(null);
		when(customerSavingAccountRepository.findByCustomer(customer)).thenReturn(customerSavingAccount);
		when(mortgageAccountRepository.findByCustomer(customer)).thenReturn(mortgageAccountSummary);
		transactionSummaryResponseDTO = new TransactionSummaryResponseDTO();
		transactionSummaryResponseDTO.setMortgageAccNumber("1");
		transactionSummaryResponseDTO.setAccountBalance(50000.0);
		transactionSummaryResponseDTO.setTransactionalAccBalance(20000.0);

		when(transactionSummaryServiceImpl.getTransactionsSummary("PKJ100")).thenReturn(transactionSummaryResponseDTO);
		ResponseEntity<TransactionSummaryResponseDTO> transactionSummaryResponseDTO = customerController
				.customerTransactionSummary("PKJ100");

		assertEquals(200, transactionSummaryResponseDTO.getStatusCodeValue());
		assertEquals(HttpStatus.OK, transactionSummaryResponseDTO.getStatusCode());

	}

	@Test
	public void testGetTransactionDetailsSummaryFailure1() {
		Customer customer = new Customer();
		customer.setCustId(1);
		customer.setCustomerId("PKJ100");
		customer.setEmail("prabir@gmail.com");
		customer.setCustomerEmployeeType("employed");
		customer.setMobileNumber("1232312312");

		CustomerSavingAccount customerSavingAccount = new CustomerSavingAccount();
		customerSavingAccount.setCustomerSavingAccId(1);
		customerSavingAccount.setAccountType("Savings Account");
		customerSavingAccount.setBalance(100000.0);
		customerSavingAccount.setCreatedDate(LocalDateTime.now());
		customerSavingAccount.setCustomerAccountNumber("1");
		customerSavingAccount.setCustomer(customer);

		MortgageAccount mortgageAccountSummary = new MortgageAccount();
		mortgageAccountSummary.setMortgageAccId(1);
		mortgageAccountSummary.setAccountBalance(100000.0);
		mortgageAccountSummary.setCreatedDate(LocalDateTime.now());
		mortgageAccountSummary.setCustomer(customer);
		mortgageAccountSummary.setMortgageAccNumber("MTG100");
		when(customerRepository.findByCustomerId("PKJ100")).thenReturn(customer);
		when(customerSavingAccountRepository.findByCustomer(customer)).thenReturn(null);
		when(mortgageAccountRepository.findByCustomer(customer)).thenReturn(mortgageAccountSummary);

		transactionSummaryResponseDTO = new TransactionSummaryResponseDTO();
		transactionSummaryResponseDTO.setMortgageAccNumber("1");
		transactionSummaryResponseDTO.setAccountBalance(50000.0);
		transactionSummaryResponseDTO.setTransactionalAccBalance(20000.0);

		when(transactionSummaryServiceImpl.getTransactionsSummary("PKJ100")).thenReturn(transactionSummaryResponseDTO);
		ResponseEntity<TransactionSummaryResponseDTO> transactionSummaryResponseDTO = customerController
				.customerTransactionSummary("PKJ100");

		assertEquals(200, transactionSummaryResponseDTO.getStatusCodeValue());
		assertEquals(HttpStatus.OK, transactionSummaryResponseDTO.getStatusCode());

	}

	@Test
	public void testGetTransactionDetailsSummaryFailure2() {
		Customer customer = new Customer();
		customer.setCustId(1);
		customer.setCustomerId("PKJ100");
		customer.setEmail("prabir@gmail.com");
		customer.setCustomerEmployeeType("employed");
		customer.setMobileNumber("1232312312");

		CustomerSavingAccount customerSavingAccount = new CustomerSavingAccount();
		customerSavingAccount.setCustomerSavingAccId(1);
		customerSavingAccount.setAccountType("Savings Account");
		customerSavingAccount.setBalance(100000.0);
		customerSavingAccount.setCreatedDate(LocalDateTime.now());
		customerSavingAccount.setCustomerAccountNumber("1");
		customerSavingAccount.setCustomer(customer);

		MortgageAccount mortgageAccountSummary = new MortgageAccount();
		mortgageAccountSummary.setMortgageAccId(1);
		mortgageAccountSummary.setAccountBalance(100000.0);
		mortgageAccountSummary.setCreatedDate(LocalDateTime.now());
		mortgageAccountSummary.setCustomer(customer);
		mortgageAccountSummary.setMortgageAccNumber("MTG100");
		when(customerRepository.findByCustomerId("PKJ100")).thenReturn(customer);
		when(customerSavingAccountRepository.findByCustomer(customer)).thenReturn(customerSavingAccount);
		when(mortgageAccountRepository.findByCustomer(customer)).thenReturn(null);

		transactionSummaryResponseDTO = new TransactionSummaryResponseDTO();
		transactionSummaryResponseDTO.setMortgageAccNumber("1");
		transactionSummaryResponseDTO.setAccountBalance(50000.0);
		transactionSummaryResponseDTO.setTransactionalAccBalance(20000.0);

		when(transactionSummaryServiceImpl.getTransactionsSummary("PKJ100")).thenReturn(transactionSummaryResponseDTO);
		ResponseEntity<TransactionSummaryResponseDTO> transactionSummaryResponseDTO = customerController
				.customerTransactionSummary("PKJ100");

		assertEquals(200, transactionSummaryResponseDTO.getStatusCodeValue());
		assertEquals(HttpStatus.OK, transactionSummaryResponseDTO.getStatusCode());

	}
}
