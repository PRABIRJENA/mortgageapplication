package com.mortgage.loan.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mortgage.loan.demo.dto.TransactionDetailsResponseDTO;
import com.mortgage.loan.demo.dto.TransactionSummaryResponseDTO;
import com.mortgage.loan.demo.entity.Customer;
import com.mortgage.loan.demo.entity.CustomerSavingAccount;
import com.mortgage.loan.demo.entity.MortgageAccount;
import com.mortgage.loan.demo.entity.MortgagePropertyDetails;
import com.mortgage.loan.demo.entity.TransactionDetails;
import com.mortgage.loan.demo.repository.CustomerRepository;
import com.mortgage.loan.demo.repository.CustomerSavingAccountRepository;
import com.mortgage.loan.demo.repository.MortgageAccountRepository;
import com.mortgage.loan.demo.repository.MortgagePropertyDetailsRepository;
import com.mortgage.loan.demo.repository.TransactionDetailsRepository;

@Service
public class TransactionSummaryServiceImpl implements TransactionSummaryService {

	@Autowired
	TransactionDetailsRepository transactionDetailsRepository;

	@Autowired
	MortgageAccountRepository mortgageAccountRepository;

	@Autowired
	MortgagePropertyDetailsRepository mortgagePropertyDetailsRepository;

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	CustomerSavingAccountRepository customerSavingAccountRepository;

	@Override
	public TransactionSummaryResponseDTO getTransactionsSummary(String customerId) {

		TransactionSummaryResponseDTO transactionSummaryResponseDTO = new TransactionSummaryResponseDTO();
		Customer customer = customerRepository.findByCustomerId(customerId);
		// TransactionDetails transactionDetailsSummary =
		// transactionDetailsRepository.findByCustomer(customer);
		CustomerSavingAccount customerSavingAccount = customerSavingAccountRepository.findByCustomer(customer);
		MortgageAccount mortgageAccountSummary = mortgageAccountRepository.findByCustomer(customer);
		/*
		 * if (transactionDetailsSummary != null) { transactionSummaryResponseDTO
		 * .setTransactionalAccNumber(transactionDetailsSummary.
		 * getTransactionalAccNumber()); transactionSummaryResponseDTO
		 * .setTransactionalAccBalance(transactionDetailsSummary.
		 * getTransactionalAccBalance()); }
		 */

		if (customerSavingAccount != null) {
			transactionSummaryResponseDTO.setTransactionalAccBalance(customerSavingAccount.getBalance());
		}

		if (mortgageAccountSummary != null) {
			transactionSummaryResponseDTO.setMortgageAccNumber(mortgageAccountSummary.getMortgageAccNumber());
			transactionSummaryResponseDTO.setAccountBalance(mortgageAccountSummary.getAccountBalance());
		}

		return transactionSummaryResponseDTO;
	}

	@Override
	public List<TransactionDetailsResponseDTO> getTransactionDetails(String customerId) {

		TransactionDetailsResponseDTO transactionDetailsResponseDTO = null;
		Customer customer = customerRepository.findByCustomerId(customerId);
		MortgageAccount mortgageAccount = mortgageAccountRepository.findByCustomer(customer);
		List<TransactionDetailsResponseDTO> transactionDetailsResponseDTOList = new ArrayList<>();
		List<TransactionDetails> transactionDetailsSummary = transactionDetailsRepository.findByCustId(customer.getCustId());
		MortgagePropertyDetails mortgagePropertyDetails = mortgagePropertyDetailsRepository
				.findByMortgageAccount(mortgageAccount);

		if (transactionDetailsSummary != null) {
			for (TransactionDetails transactionDetails : transactionDetailsSummary) {
				transactionDetailsResponseDTO = new TransactionDetailsResponseDTO();
				transactionDetailsResponseDTO.setTransactionDate(transactionDetails.getTranscationDate());
				transactionDetailsResponseDTO.setTransComment(transactionDetails.getTransComment());
				if (mortgageAccount != null) {
					transactionDetailsResponseDTO.setMortgageCreatedDate(mortgageAccount.getCreatedDate());
					transactionDetailsResponseDTO.setMortgageAccountBalance(mortgageAccount.getAccountBalance());
				}
				if (mortgagePropertyDetails != null) {
					transactionDetailsResponseDTO.setMortgageComment(mortgagePropertyDetails.getComment());
				}
				transactionDetailsResponseDTOList.add(transactionDetailsResponseDTO);
			}
		}
		return transactionDetailsResponseDTOList;
	}
}
