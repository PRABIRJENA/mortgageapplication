package com.mortgage.loan.demo.service;

import com.mortgage.loan.demo.dto.CustomerLoginReqDto;
import com.mortgage.loan.demo.dto.CustomerLoginResponseDto;

public interface CustomerService {
	public CustomerLoginResponseDto customerLogin(CustomerLoginReqDto reqDto);
}
