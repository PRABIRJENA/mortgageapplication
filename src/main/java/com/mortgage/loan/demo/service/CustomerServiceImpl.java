package com.mortgage.loan.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mortgage.loan.demo.constants.AppConstants;
import com.mortgage.loan.demo.dto.CustomerLoginReqDto;
import com.mortgage.loan.demo.dto.CustomerLoginResponseDto;
import com.mortgage.loan.demo.entity.Customer;
import com.mortgage.loan.demo.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository repository;

	@Override
	public CustomerLoginResponseDto customerLogin(CustomerLoginReqDto reqDto) {
		CustomerLoginResponseDto response = new CustomerLoginResponseDto();
		Customer customer = repository.findByCustomerIdAndPassword(reqDto.getCustomerId(), reqDto.getPassword());
		if (customer != null) {
			response.setStatusCode(AppConstants.LOGIN_SUCCESS_STATUS_CODE);
			response.setStatusMsg(AppConstants.LOGIN_SUCCESS);
		} else {
			response.setStatusCode(AppConstants.LOGIN_FAILURE_STATUS_CODE);
			response.setStatusMsg(AppConstants.LOGIN_FAILURE);
		}
		return response;
	}

}
