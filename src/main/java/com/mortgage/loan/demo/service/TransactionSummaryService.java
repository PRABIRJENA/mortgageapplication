package com.mortgage.loan.demo.service;

import java.util.List;

import com.mortgage.loan.demo.dto.TransactionDetailsResponseDTO;
import com.mortgage.loan.demo.dto.TransactionSummaryResponseDTO;

public interface TransactionSummaryService {

	TransactionSummaryResponseDTO getTransactionsSummary(String customerId);

	List<TransactionDetailsResponseDTO> getTransactionDetails(String customerId);
}
