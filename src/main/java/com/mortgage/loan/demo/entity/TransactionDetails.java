package com.mortgage.loan.demo.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TRANSACTIONAL_DETAILS")
public class TransactionDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer transactionAccId;
	@OneToOne
	@JoinColumn(name = "custId")
	private Customer customer;
	private LocalDateTime transcationDate;
	private String transactionType;
	private String transComment;
	private double transactionalAccBalance;

	public double getTransactionalAccBalance() {
		return transactionalAccBalance;
	}

	public void setTransactionalAccBalance(double transactionalAccBalance) {
		this.transactionalAccBalance = transactionalAccBalance;
	}

	public TransactionDetails() {
		super();
	}

	public Integer getTransactionAccId() {
		return transactionAccId;
	}

	public void setTransactionAccId(Integer transactionAccId) {
		this.transactionAccId = transactionAccId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public LocalDateTime getTranscationDate() {
		return transcationDate;
	}

	public void setTranscationDate(LocalDateTime transcationDate) {
		this.transcationDate = transcationDate;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransComment() {
		return transComment;
	}

	public void setTransComment(String transComment) {
		this.transComment = transComment;
	}

	@Override
	public String toString() {
		return "TransactionDetails [transactionAccId=" + transactionAccId + ", customer=" + customer
				+ ", transcationDate=" + transcationDate + ", transactionType=" + transactionType + ", transComment="
				+ transComment + ", transactionalAccBalance=" + transactionalAccBalance + "]";
	}

}
