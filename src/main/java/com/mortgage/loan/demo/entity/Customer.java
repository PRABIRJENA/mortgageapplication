package com.mortgage.loan.demo.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "CUSTOMER")
public class Customer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy =  GenerationType.AUTO)
	private Integer custId;
	private String customerId;
	private String customerFirstName;
	private String customerMiddleName;
	private String customerSurName;
	private LocalDateTime dob;
	private String email;
	private String customerOccupation;
	private LocalDateTime occupationDoj;
	private String mobileNumber;
	private String customerEmployeeType;
	private double salary;
	private String password;
	private String contractType;
	private String customerTitle;
	

	public Customer() {
		super();
	}

	public Integer getCustId() {
		return custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getCustomerMiddleName() {
		return customerMiddleName;
	}

	public void setCustomerMiddleName(String customerMiddleName) {
		this.customerMiddleName = customerMiddleName;
	}

	public String getCustomerSurName() {
		return customerSurName;
	}

	public void setCustomerSurName(String customerSurName) {
		this.customerSurName = customerSurName;
	}

	public LocalDateTime getDob() {
		return dob;
	}

	public void setDob(LocalDateTime dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCustomerOccupation() {
		return customerOccupation;
	}

	public void setCustomerOccupation(String customerOccupation) {
		this.customerOccupation = customerOccupation;
	}

	public LocalDateTime getOccupationDoj() {
		return occupationDoj;
	}

	public void setOccupationDoj(LocalDateTime occupationDoj) {
		this.occupationDoj = occupationDoj;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCustomerEmployeeType() {
		return customerEmployeeType;
	}

	public void setCustomerEmployeeType(String customerEmployeeType) {
		this.customerEmployeeType = customerEmployeeType;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getCustomerTitle() {
		return customerTitle;
	}

	public void setCustomerTitle(String customerTitle) {
		this.customerTitle = customerTitle;
	}

	@Override
	public String toString() {
		return "Customer [custId=" + custId + ", customerId=" + customerId + ", customerFirstName=" + customerFirstName
				+ ", customerMiddleName=" + customerMiddleName + ", customerSurName=" + customerSurName + ", dob=" + dob
				+ ", email=" + email + ", customerOccupation=" + customerOccupation + ", occupationDoj=" + occupationDoj
				+ ", mobileNumber=" + mobileNumber + ", customerEmployeeType=" + customerEmployeeType + ", salary="
				+ salary + ", password=" + password + "]";
	}
}
