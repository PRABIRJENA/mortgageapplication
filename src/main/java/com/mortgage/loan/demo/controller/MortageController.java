package com.mortgage.loan.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mortgage.loan.demo.dto.MortgageDto;
import com.mortgage.loan.demo.dto.MortgageResponseDto;
import com.mortgage.loan.demo.exception.AgeValidException;
import com.mortgage.loan.demo.exception.CustomerExistException;
import com.mortgage.loan.demo.exception.DepositValueException;
import com.mortgage.loan.demo.exception.MinimumPropertyCostException;
import com.mortgage.loan.demo.service.MortgageService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("Operations pertaining to Mortgage ")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MortageController {

	@Autowired
	MortgageService mortgageService;
	
	@ApiOperation("mortgage registartion")
	@PostMapping("/mortgage")
	public ResponseEntity<MortgageResponseDto> mortgageRegister(@Valid @RequestBody MortgageDto mortgageDto) throws AgeValidException, CustomerExistException, MinimumPropertyCostException, DepositValueException {
		MortgageResponseDto response = mortgageService.mortgageRegister(mortgageDto);
		return new ResponseEntity<>(response,HttpStatus.CREATED);
	}
}
