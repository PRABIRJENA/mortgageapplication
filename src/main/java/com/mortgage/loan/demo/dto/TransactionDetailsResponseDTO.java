package com.mortgage.loan.demo.dto;

import java.time.LocalDateTime;

public class TransactionDetailsResponseDTO {
	private double mortgageAccountBalance;
	private String mortgageComment;
	private LocalDateTime mortgageCreatedDate;
	public double getMortgageAccountBalance() {
		return mortgageAccountBalance;
	}
	public void setMortgageAccountBalance(double mortgageAccountBalance) {
		this.mortgageAccountBalance = mortgageAccountBalance;
	}
	public String getMortgageComment() {
		return mortgageComment;
	}
	public void setMortgageComment(String mortgageComment) {
		this.mortgageComment = mortgageComment;
	}
	public LocalDateTime getMortgageCreatedDate() {
		return mortgageCreatedDate;
	}
	public void setMortgageCreatedDate(LocalDateTime mortgageCreatedDate) {
		this.mortgageCreatedDate = mortgageCreatedDate;
	}
	public LocalDateTime getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(LocalDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getTransComment() {
		return transComment;
	}
	public void setTransComment(String transComment) {
		this.transComment = transComment;
	}
	private LocalDateTime transactionDate;
	private String transComment;

	
}
