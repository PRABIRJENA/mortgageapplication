package com.mortgage.loan.demo.dto;

public class TransactionSummaryResponseDTO {

	private String mortgageAccNumber;
	private double accountBalance;
	private double transactionalAccBalance;

	public String getMortgageAccNumber() {
		return mortgageAccNumber;
	}

	public void setMortgageAccNumber(String mortgageAccNumber) {
		this.mortgageAccNumber = mortgageAccNumber;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public double getTransactionalAccBalance() {
		return transactionalAccBalance;
	}

	public void setTransactionalAccBalance(double transactionalAccBalance) {
		this.transactionalAccBalance = transactionalAccBalance;
	}
}
