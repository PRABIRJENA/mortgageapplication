package com.mortgage.loan.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mortgage.loan.demo.entity.Customer;
import com.mortgage.loan.demo.entity.TransactionDetails;

@Repository
public interface TransactionDetailsRepository extends JpaRepository<TransactionDetails, Integer> {

	public TransactionDetails findByCustomer(Customer customer);

	//@Query(value = "SELECT * FROM transactional_details where cust_id=:custId ORDER BY transcation_date DESC LIMIT 5", nativeQuery = true)
	//public List<TransactionDetails> findAllByCustomer(@Param("custId") String custId);

	@Query(value = "SELECT * FROM transactional_details where cust_id=:custId ORDER BY transcation_date DESC LIMIT 5", nativeQuery = true)
	public List<TransactionDetails> findByCustId(Integer custId);
}
