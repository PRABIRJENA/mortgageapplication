package com.mortgage.loan.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mortgage.loan.demo.entity.MortgageAccount;
import com.mortgage.loan.demo.entity.MortgagePropertyDetails;

@Repository
public interface MortgagePropertyDetailsRepository extends JpaRepository<MortgagePropertyDetails, Integer> {

	MortgagePropertyDetails findByMortgageAccount(MortgageAccount mortgageAccount);

}
