package com.mortgage.loan.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mortgage.loan.demo.entity.Customer;
import com.mortgage.loan.demo.entity.MortgageAccount;



@Repository
public interface MortgageAccountRepository extends JpaRepository<MortgageAccount, Integer> {

	MortgageAccount findByCustomer(Customer customer);

}
